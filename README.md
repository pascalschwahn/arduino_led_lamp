#Arduino LED Bluetooth and Music Visualisation Lamp
--------------------------------------------------
See [Wiki](https://bitbucket.org/pascalschwahn/arduino_led_lamp/wiki/Home) for more information.

##Wiki links
--------------------------------------------------
1. [Construction](https://bitbucket.org/pascalschwahn/arduino_led_lamp/wiki/Construction)
    * [Components](https://bitbucket.org/pascalschwahn/arduino_led_lamp/wiki/Construction#markdown-header-components)
    * [Schematics](https://bitbucket.org/pascalschwahn/arduino_led_lamp/wiki/Construction#markdown-header-schematics)
2. [Bluetooth API](https://bitbucket.org/pascalschwahn/arduino_led_lamp/wiki/Bluetooth%20API)
    * [off](https://bitbucket.org/pascalschwahn/arduino_led_lamp/wiki/off)
    * [fade](https://bitbucket.org/pascalschwahn/arduino_led_lamp/wiki/fade)
    * [brightness](https://bitbucket.org/pascalschwahn/arduino_led_lamp/wiki/brightness)
    * [setcolor](https://bitbucket.org/pascalschwahn/arduino_led_lamp/wiki/setcolor)
    * [aux](https://bitbucket.org/pascalschwahn/arduino_led_lamp/wiki/aux)
    * [mic](https://bitbucket.org/pascalschwahn/arduino_led_lamp/wiki/mic)
    * [snowsparkle](https://bitbucket.org/pascalschwahn/arduino_led_lamp/wiki/snowsparkle)
    * [vudelay](https://bitbucket.org/pascalschwahn/arduino_led_lamp/wiki/vudelay)
    * [auxsensitivity](https://bitbucket.org/pascalschwahn/arduino_led_lamp/wiki/auxsensitivity)
    * [micsensitivity](https://bitbucket.org/pascalschwahn/arduino_led_lamp/wiki/micsensitivity)
    * [segmentsize](https://bitbucket.org/pascalschwahn/arduino_led_lamp/wiki/segmentsize)