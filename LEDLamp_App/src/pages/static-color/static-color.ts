import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LEDLamp } from '../../services/lamp';
import { BluetoothService } from '../../services/bluetooth';
//https://rakujira.jp/projects/iro/docs/guide.html#Color-Picker-Options
import iro from '@jaames/iro';

/**
 * Generated class for the StaticColorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-static-color',
  templateUrl: 'static-color.html',
})
export class StaticColorPage {
  private color: iro.Color;
  private activated: boolean;
  private isFade: boolean;
  private fade_speed: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, private lamp: LEDLamp, private bluetooth: BluetoothService) {
    this.activated = false;
    this.color = new iro.Color({r:0, g:0, b:255});
    this.isFade = false;
    this.fade_speed = 20;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StaticColorPage');
  }
  
  setColor(color, changes){
    this.color = color;
    if(changes.v){
      this.lamp.setBrightness(color.hsv.v);
    }
    if(!this.isFade){
      this.lamp.setColor(color);
      this.activated = true;
    }
  }

  resume(){
    if(!this.isFade){
      this.lamp.setColor(this.color);
    }else{
      this.lamp.fade(this.fade_speed);
    }
    this.lamp.setBrightness(this.color.hsv.v);
  }

  toggleFade(){
    if(this.isFade){
      this.fade();
    }else{
      this.lamp.setColor(this.color);
      this.activated = true;
    }
  }

  fade(){
    this.activated = true;
    this.lamp.fade(this.fade_speed);
  }

}
