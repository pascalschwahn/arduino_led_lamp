import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StaticColorPage } from './static-color';
import { BluetoothComponentModule } from '../../components/bluetooth/bluetooth.module';
import { ColorPickerComponentModule } from '../../components/color-picker/color-picker.module';
import { NavbarComponentModule } from '../../components/navbar/navbar.module';

@NgModule({
  declarations: [
    StaticColorPage,
  ],
  imports: [
    IonicPageModule.forChild(StaticColorPage),
    BluetoothComponentModule,
    ColorPickerComponentModule,
    NavbarComponentModule
  ],
})
export class StaticColorPageModule {}
