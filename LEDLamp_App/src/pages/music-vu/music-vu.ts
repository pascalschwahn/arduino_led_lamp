import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LEDLamp } from '../../services/lamp';
import { BluetoothService } from '../../services/bluetooth';
//https://rakujira.jp/projects/iro/docs/guide.html#Color-Picker-Options
import iro from '@jaames/iro';

/**
 * Generated class for the MusicVuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-music-vu',
  templateUrl: 'music-vu.html',
})
export class MusicVuPage {
  private isAux: boolean;
  private mode: string;
  private vu_speed: number;
  private color: iro.Color;
  private activated: boolean;
  private mic_sensitivity: number;
  private mic_bandwidth: number;
  private vu_delay: number;
  private aux_sensitivity: number;
  private segment_size: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, private bluetooth: BluetoothService, private lamp: LEDLamp) {
    this.color = new iro.Color({r:0, g:0, b:255});
    this.activated = false;
    this.isAux = false;
    this.vu_speed = 100;
    this.mode = '1';
    this.mic_sensitivity = 60;
    this.mic_bandwidth = 20;
    this.vu_delay = 5;
    this.aux_sensitivity = 50;
    this.segment_size = 1;
  }

  segmentSize(){
    this.lamp.segmentsize(this.segment_size);
  }

  micSensitivity(){
    this.lamp.micsensitivity(this.mic_sensitivity, this.mic_bandwidth);
  }

  auxSensitivity(){
    this.lamp.auxsensitivity(this.aux_sensitivity);
  }

  vuDelay(){
    this.lamp.vudelay(this.vu_delay);
  }

  private activateAux(){
    this.activated = true;
    switch(this.mode){
      case '0':
        this.lamp.aux_mode_0(this.vu_speed);
        break;
      case '1':
        this.lamp.aux_mode_1(this.color);
        break;
      case '2':
        this.lamp.aux_mode_2();
        break;
      default:
        break;
    }
  }

  private activateMic(){
    this.activated = true;
    switch(this.mode){
      case '0':
        console.log("SEND MIC 0");
        this.lamp.mic_mode_0(this.vu_speed);
        break;
      case '1':
        this.lamp.mic_mode_1(this.color);
        break;
      case '2':
        this.lamp.mic_mode_2();
        break;
      default:
        break;
    }
  }

  setColor(color, changes){
    this.color = color;
    if(changes.v){
      this.lamp.setBrightness(color.hsv.v);
    }
    this.sendCommand();
  }

  sendCommand(){
    if(this.isAux){
      this.activateAux();
    }else{
      this.activateMic();
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MusicVuPage');
  }

  resume(){
    this.sendCommand();
  }

}
