import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MusicVuPage } from './music-vu';
import { BluetoothComponentModule } from '../../components/bluetooth/bluetooth.module';
import { NavbarComponentModule } from '../../components/navbar/navbar.module';
import { ColorPickerComponentModule } from '../../components/color-picker/color-picker.module'

@NgModule({
  declarations: [
    MusicVuPage,
  ],
  imports: [
    IonicPageModule.forChild(MusicVuPage),
    BluetoothComponentModule,
    NavbarComponentModule,
    ColorPickerComponentModule
  ],
})
export class MusicVuPageModule {}
