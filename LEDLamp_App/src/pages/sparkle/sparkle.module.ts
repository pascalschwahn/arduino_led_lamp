import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SparklePage } from './sparkle';
import { BluetoothComponentModule } from '../../components/bluetooth/bluetooth.module';
import { NavbarComponentModule } from '../../components/navbar/navbar.module';
import { ColorPickerComponentModule } from '../../components/color-picker/color-picker.module';

@NgModule({
  declarations: [
    SparklePage,
  ],
  imports: [
    IonicPageModule.forChild(SparklePage),
    BluetoothComponentModule,
    NavbarComponentModule,
    ColorPickerComponentModule
  ],
})
export class SparklePageModule {}
