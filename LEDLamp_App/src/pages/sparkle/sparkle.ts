import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BluetoothService } from '../../services/bluetooth';
import { LEDLamp } from '../../services/lamp';
//https://rakujira.jp/projects/iro/docs/guide.html#Color-Picker-Options
import iro from '@jaames/iro';

/**
 * Generated class for the SparklePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sparkle',
  templateUrl: 'sparkle.html',
})
export class SparklePage {

  private num_sparkles: number;
  private sparkle_speed: number;
  private sparkle_brightness_diff: number;
  private color: iro.Color;
  private activated: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, private bluetooth: BluetoothService, private lamp: LEDLamp) {
    this.num_sparkles = 3;
    this.sparkle_speed = 70;
    this.sparkle_brightness_diff = 90;
    this.color = new iro.Color({r:0, g:0, b:255});
    this.activated = false;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SparklePage');
  }

  setColor(color, changes){
    this.color = color;
    if(changes.v){
      this.lamp.setBrightness(color.hsv.v);
    }
    this.activateSparkle();
  }

  activateSparkle(){
    this.activated = true;
    this.lamp.snowSparkle(this.num_sparkles, this.sparkle_speed, this.sparkle_brightness_diff, this.color);
  }

  resume(){
    this.lamp.snowSparkle(this.num_sparkles, this.sparkle_speed, this.sparkle_brightness_diff, this.color);
  }

}
