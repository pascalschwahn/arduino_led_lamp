import { BluetoothService } from './bluetooth';
import { Injectable } from '@angular/core';
//https://rakujira.jp/projects/iro/docs/guide.html#Color-Picker-Options
import iro from '@jaames/iro';

@Injectable()
export class LEDLamp{

    private lastMode: string;
    private activeColor: iro.Color;
    private brightness: number;
    private fade_speed: number;
    private vu_speed: number;
    private num_sparkles: number;
    private sparkle_speed: number;
    private sparkle_brightness_diff: number;
    private on: boolean;
    private vu_delay: number;
    private aux_sensitivity: number;
    private mic_sensitivity: number;
    private mic_bandwidth: number;
    private segment_size: number;

    constructor(private bluetooth: BluetoothService){
        this.activeColor = new iro.Color({r:0, g:0, b:255});
        this.brightness = 25;
        this.fade_speed = 20;
        this.vu_speed = 100;
        this.lastMode = "setColor";
        this.num_sparkles = 3;
        this.sparkle_speed = 70;
        this.sparkle_brightness_diff = 90;
        this.on = true;
        this.vu_delay = 5;
        this.aux_sensitivity = 50;
        this.mic_sensitivity = 60,
        this.mic_bandwidth = 20;
        this.segment_size = 1;
    }

    /**
     * Helpmethod to set activeColor and color_with_brightness
     * @param color iro.Color
     */
    private setActiveColor(color: iro.Color){
        this.brightness = color.hsv.v;
        this.activeColor = new iro.Color({r: color.rgb.r, g: color.rgb.g, b: color.rgb.b});
        //eleminate the brightness value
        this.activeColor.setChannel("hsv", "v", 100);
    }

    /**
     * True if the lamp is currently on
     */
    isOn(){
        return this.on;
    }

    /**
     * Sets the static color of the lamp
     * @param color iro.Color object
     */
    setColor(color: iro.Color){
        this.lastMode = "setColor";
        this.setActiveColor(color);
        this.bluetooth.sendCommand("<setcolor:" + this.activeColor.rgb.r + ":" + this.activeColor.rgb.g + ":" + this.activeColor.rgb.b + ">");
        this.on = true;
    }

    /**
     * Changes the brighntess of the led lamp
     * @param b brigthness in percent (0-100)
     */
    setBrightness(b: number){
        this.brightness = b;
        this.bluetooth.sendCommand("<brightness:" + Math.round(b * 255/100) + ">");
        this.on = true;
    }

    /**
     * Turns all leds off
     */
    off(){
        this.bluetooth.sendCommand("<off>");
        this.on = false;
    }

    /**
     * Activates the fading mode
     * @param speed Fading speed in milliseconds.
     */
    fade(speed: number){
        this.lastMode = "fade";
        this.fade_speed = speed;
        this.bluetooth.sendCommand("<fade:" + speed + ">");
        this.on = true;
    }

    /**
     * Activates aux music visualization with active color schema in running animation -> from bottom to top
     * @param speed color changing speed in milliseconds.
     */
    aux_mode_0(speed: number){
        this.lastMode = "aux_mode_0";
        this.vu_speed = speed;
        this.bluetooth.sendCommand("<aux:0:" + speed + ">");
        this.on = true;
    }

    /**
     * Activates aux music visualization with static color
     * @param color iro.Color object
     */
    aux_mode_1(color: iro.Color){
        this.lastMode = "aux_mode_1";
        this.setActiveColor(color);
        this.bluetooth.sendCommand("<aux:1:" + this.activeColor.rgb.r + ":" + this.activeColor.rgb.g + ":" + this.activeColor.rgb.b + ">");
        this.on = true;
    }

    /**
     * Activates aux music visualization with active static color schema
     */
    aux_mode_2(){
        this.lastMode = "aux_mode_2";
        this.bluetooth.sendCommand("<aux:2>");
        this.on = true;
    }

    /**
     * Activates mic music visualization with active color schema in running animation -> from bottom to top
     * @param speed color changing speed in milliseconds.
     */
    mic_mode_0(speed: number){
        this.vu_speed = speed;
        this.lastMode = "mic_mode_0";
        this.bluetooth.sendCommand("<mic:0:" + speed + ">");
        this.on = true;
    }

    /**
     * Activates mic music visualization with static color
     * @param color iro.Color object
     */
    mic_mode_1(color: iro.Color){
        this.lastMode = "mic_mode_1";
        this.setActiveColor(color);
        this.bluetooth.sendCommand("<mic:1:" + this.activeColor.rgb.r + ":" + this.activeColor.rgb.g + ":" + this.activeColor.rgb.b + ">");
        this.on = true;
    }

    /**
     * Activates mic music visualization with active static color schema
     */
    mic_mode_2(){
        this.lastMode = "mic_mode_2";
        this.bluetooth.sendCommand("<mic:2>");
        this.on = true;
    }

    /**
     * Activates sparkle mode
     * @param num_sparkles How many sparkles at the same time
     * @param speed Sparkle speed in milliseconds
     * @param color iro.Color
     */
    sparkle(num_sparkles: number, speed: number, color: iro.Color){
        this.num_sparkles = num_sparkles;
        this.sparkle_speed = speed;
        this.lastMode = "sparkle";
        this.setActiveColor(color);
        this.bluetooth.sendCommand("<sparkle:" + num_sparkles + ":" + speed + ":" + this.activeColor.rgb.r + ":" + this.activeColor.rgb.g + ":" + this.activeColor.rgb.b + ">");
        this.on = true;
    }

    /**
     * Activates snowsparkle mode
     * @param num_sparkles How many sparkles at the same time
     * @param speed Sparkle speed in milliseconds
     * @param brighntess_diff brightness difference between sparkle and no sparkle in percent (0-100)
     * @param color iro.Color
     */
    snowSparkle(num_sparkles: number, speed: number, brighntess_diff: number, color: iro.Color){
        this.num_sparkles = num_sparkles;
        this.sparkle_speed = speed;
        this.sparkle_brightness_diff = brighntess_diff;
        this.lastMode = "snowSparkle";
        this.setActiveColor(color);
        this.bluetooth.sendCommand("<snowsparkle:" + num_sparkles + ":" + speed + ":" + brighntess_diff + ":" + this.activeColor.rgb.r + ":" + this.activeColor.rgb.g + ":" + this.activeColor.rgb.b + ">");
        this.on = true;
    }

    /**
     * Sets the music vu delay for smoother animation
     * @param delay delay in milliseconds
     */
    vudelay(delay: number){
        this.vu_delay = delay;
        this.bluetooth.sendCommand("<vudelay:" + delay + ">");
    }

    /**
     * Sets the aux sensitivity
     * @param sensitivity sensitivity for aux animation
     */
    auxsensitivity(sensitivity: number){
        this.aux_sensitivity = sensitivity;
        this.bluetooth.sendCommand("<auxsensitivity:" + sensitivity + ">");
    }

    /**
     * Sets the mic sensitivity and bandwidth
     * @param sensitivity sensitivity for mic animation
     * @param bandwidth bandwidth for mic animation
     */
    micsensitivity(sensitivity: number, bandwidth: number){
        this.mic_bandwidth = bandwidth;
        this.mic_sensitivity = sensitivity;
        this.bluetooth.sendCommand("<micsensitivity:" + sensitivity + ":" + bandwidth + ">");
    }

    /**
     * Sets the segment size
     * @param size number of leds per segment
     */
    segmentsize(size: number){
        this.segment_size = size;
        this.bluetooth.sendCommand("<segmentsize:" + size + ">");
    }

    /**
     * Resumes the last lamp mode and turns the leds on
     */
    resume(){
        switch(this.lastMode){
            case "snowSparkle":
                this.activeColor.setChannel("hsv", "v", this.brightness);
                this.snowSparkle(this.num_sparkles, this.sparkle_speed, this.sparkle_brightness_diff, this.activeColor);
                break;
            case "sparkle":
                this.activeColor.setChannel("hsv", "v", this.brightness);
                this.sparkle(this.num_sparkles, this.sparkle_speed, this.activeColor);
                break;
            case "mic_mode_2":
                this.mic_mode_2();
                break;
            case "mic_mode_1":
                this.activeColor.setChannel("hsv", "v", this.brightness);
                this.mic_mode_1(this.activeColor);
                break;
            case "mic_mode_0":
                this.mic_mode_0(this.vu_speed);
                break;
            case "aux_mode_2":
                this.aux_mode_2();
                break;
            case "aux_mode_1":
                this.activeColor.setChannel("hsv", "v", this.brightness);
                this.aux_mode_1(this.activeColor);
                break;
            case "aux_mode_0":
                this.aux_mode_0(this.vu_speed);
                break;
            case "fade":
                this.fade(this.fade_speed);
                break;
            case "setColor":
                this.activeColor.setChannel("hsv", "v", this.brightness);
                this.setColor(this.activeColor);
                break;
            default:
                break;
        }
    }
}