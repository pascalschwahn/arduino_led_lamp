import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { AlertController, Events } from 'ionic-angular';
import { Injectable, NgZone } from '@angular/core';

@Injectable()
export class BluetoothService{
    private unpairedDevices: any;
    private gettingDevices: boolean;

    private connected: boolean;

    constructor(private bluetoothSerial: BluetoothSerial, private alertCtrl: AlertController, private events: Events, private zone: NgZone){
        //Enable Bluetooth on Device
        this.connected = false;
        bluetoothSerial.enable();
        this.checkConnection();
        //Solve DOM update problem when connecting
        this.events.subscribe('updateScreen', () => {
            this.zone.run(() => {
                console.log('force update the screen');
            });
        });
    }

    isConnected(): boolean{
        return this.connected;
    }

    checkConnection(){
        this.bluetoothSerial.isConnected().then(() => {
            this.connected = true;
        },
        ()=>{
            this.connected = false;
        });
    }

    sendCommand(command: string, success?, failure?){
        this.bluetoothSerial.write(command).then(success, failure);
    }

    startScanning() {
        this.unpairedDevices = null;
        this.gettingDevices = true;
        this.bluetoothSerial.discoverUnpaired().then((success) => {
            this.unpairedDevices = success;
            this.gettingDevices = false;
            success.forEach(element => {
            // alert(element.name);
            });
        },
            (err) => {
            console.log(err);
            })
    }

    success = (data) => {this.connected = true; this.events.publish('updateScreen');};
    fail = (error) => {alert(error); this.connected = false; this.events.publish('updateScreen');};

    selectDevice(address: any) {
        let alert = this.alertCtrl.create({
            title: 'Connect',
            message: 'Do you want to connect with?',
            buttons: [
            {
                text: 'Cancel',
                role: 'cancel',
                handler: () => {
                console.log('Cancel clicked');
                }
            },
            {
                text: 'Connect',
                handler: () => {
                this.bluetoothSerial.connect(address).subscribe(this.success, this.fail);
                }
            }
            ]
        });
        alert.present();
    }

    disconnect() {
        let alert = this.alertCtrl.create({
            title: 'Disconnect?',
            message: 'Do you want to Disconnect?',
            buttons: [
            {
                text: 'Cancel',
                role: 'cancel',
                handler: () => {
                console.log('Cancel clicked');
                }
            },
            {
                text: 'Disconnect',
                handler: () => {
                this.bluetoothSerial.disconnect();
                this.connected = false;
                }
            }
            ]
        });
        alert.present();
    }
}