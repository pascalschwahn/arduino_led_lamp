import { Component, Input } from '@angular/core';
import { LEDLamp } from '../../services/lamp';
import { BluetoothService } from '../../services/bluetooth';

/**
 * Generated class for the NavbarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'navbar',
  templateUrl: 'navbar.html'
})
export class NavbarComponent {

  @Input() title: string;

  constructor(private lamp: LEDLamp, private bluetooth: BluetoothService) {
    console.log('Hello NavbarComponent Component');
    this.title = 'PJS LED Lamp';
  }

  power(){
    if(!this.lamp.isOn()){
      this.lamp.resume();
    }else{
      this.lamp.off();
    }
  }

  disconnect(){
    this.bluetooth.disconnect();
  }

}
