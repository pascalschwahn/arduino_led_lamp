import { NgModule } from '@angular/core';
import { BluetoothComponent } from './bluetooth/bluetooth';
import { IonicModule } from 'ionic-angular';
import { ColorPickerComponent } from './color-picker/color-picker';
import { NavbarComponent } from './navbar/navbar';
@NgModule({
	declarations: [BluetoothComponent,
    ColorPickerComponent,
    NavbarComponent],
	imports: [IonicModule],
	exports: [BluetoothComponent,
    ColorPickerComponent,
    NavbarComponent]
})
export class ComponentsModule {}
