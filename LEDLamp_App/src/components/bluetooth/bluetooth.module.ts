import { NgModule } from '@angular/core';
import { BluetoothComponent } from './bluetooth';
import { IonicModule } from 'ionic-angular';
@NgModule({
	declarations: [BluetoothComponent],
	imports: [IonicModule],
	exports: [BluetoothComponent]
})
export class BluetoothComponentModule {}
