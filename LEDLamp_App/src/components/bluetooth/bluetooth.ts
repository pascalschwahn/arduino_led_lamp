import { Component } from '@angular/core';
import { BluetoothService } from '../../services/bluetooth';

/**
 * Generated class for the BluetoothComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'bluetooth',
  templateUrl: 'bluetooth.html'
})
export class BluetoothComponent {

  constructor(private bluetooth: BluetoothService) {
    console.log('Hello BluetoothComponent Component');
    console.log(this.bluetooth.isConnected());
  }

}
