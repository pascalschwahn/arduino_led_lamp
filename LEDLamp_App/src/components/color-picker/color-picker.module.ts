import { NgModule } from '@angular/core';
import { ColorPickerComponent } from './color-picker';
@NgModule({
	declarations: [ColorPickerComponent],
	imports: [],
	exports: [ColorPickerComponent]
})
export class ColorPickerComponentModule {}
