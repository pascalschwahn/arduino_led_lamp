import { Component, Output, EventEmitter, Input } from '@angular/core';
import {Platform} from 'ionic-angular';
//https://rakujira.jp/projects/iro/docs/guide.html#Color-Picker-Options
import iro from '@jaames/iro';

/**
 * Generated class for the ColorPickerComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'color-picker',
  templateUrl: 'color-picker.html'
})
export class ColorPickerComponent {
  @Output() onChange: EventEmitter<any> = new EventEmitter();
  @Input() picker_id: string;
  

  private colorPicker: iro.ColorPicker;

  constructor(private platform: Platform) {
    console.log('Hello ColorPickerComponent Component');
    platform.ready().then((readySource) => {
      this.colorPicker = new iro.ColorPicker("#" + this.picker_id,{
        width: platform.width()-33,
        height: platform.width()-33,
        padding: 0,
        sliderHeight: 36,
        markerRadius: 12,
        wheelLightness: false
      });
      //EventListener
      this.colorPicker.on("color:change", (color, changes) => {
        this.onChange.emit([color, changes]);
      });
    });
  }

}
