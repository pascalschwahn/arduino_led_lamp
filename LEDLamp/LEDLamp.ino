#include <FastLED.h>

#define NUM_LEDS 60
#define DEBUG

//Definition of Pins
#define LED_PIN 6
#define MIC_PIN A0
#define AUX_LEFT A1
#define AUX_RIGHT A2
#define AUX_SWITCH_1 11
#define AUX_SWITCH_2 12

#define MIC_VALUE_STEPS 10

//Bluetooth settings
const byte NUM_CHARS = 32; //max chars per bluetooth command
const char* DELIMITER = ":"; //bluetooth parameter delimiter
const char START_MARKER = '<'; //listen for command
const char END_MARKER = '>'; //command ended

char receivedChars[NUM_CHARS]; //received message over bluetooth
boolean newData = false; //is there new data?

CRGB leds[NUM_LEDS]; //led array for FastLED
CRGB leds_color_prep[NUM_LEDS]; //for color preperation in animations

//Basic settings
int method_speed = 15; //fade speed in milliseconds
byte brightness = 200; //brightness of led strip (0-255)
byte num_of_sparkles = 3; //number of sparkles
float sparkle_brightness_diff = 0.8; //brightness difference for sparkle effect (value from 0 to 1)

//State variables
byte mode = 0; //active mode
CRGB active_color = CRGB(0, 0, 255); //last active color
byte active_fade_color = 0; // 0=red; 1=green; 2=blue

//Audio Animation Settings
byte segmentSize = 1; //Size in how many LEDs are one Segment
byte numOfSegments = NUM_LEDS / segmentSize;
byte vu_mode = 2;
int aux_sensitivity = 50;
int mic_max; //max peak of mic
int mic_min; //noise level in your environment
int mic_sensitivity = 60; //sets the mic_max with MIC_VALUE_STEPS
int mic_bandwidth = 20; //sets the difference between mic_max and mic_min
int _delay = 5; //delay for smoother vu animation

unsigned long last_color_change = 0;
int color_change_speed = 100; //in milliseconds

void setup() {
  Serial.begin(9600);
  FastLED.addLeds<WS2812B, LED_PIN, GRB>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
  pinMode(LED_PIN, OUTPUT);
  pinMode(AUX_SWITCH_1, INPUT);
  pinMode(AUX_SWITCH_2, INPUT);

  mic_max = mic_sensitivity * MIC_VALUE_STEPS;
  mic_min = mic_max - mic_bandwidth * MIC_VALUE_STEPS;
  
  #ifdef DEBUG
    Serial.println("Lamp initialized");
  #endif

  FastLED.setBrightness(brightness);
  startUpAnimation();
}

void loop() {
  recvData();
  setStateVariables();

  switch(mode){
    case -1: //Error
      error();
      break;
    case 0: //Turn lamp off
      FastLED.clear(); //Turn every LED off
      FastLED.show();
      break;
    case 1: //Fade
      fade();
      break;
    case 2: //Static Color
      fill_solid(leds, NUM_LEDS, active_color);
      FastLED.show();
      break;
    case 3: //Aux Mode
      auxInput();
      break;
    case 4: //Mic Mode
      micInput();
      break;
    case 5: //Sparkle Mode
      sparkle();
      break;
    default:
      //TODO
      break;
  }
  
  newData = false; //set flag that state variables are up to date
  delay(1); //for stabilization
}

void startUpAnimation(){
  setStartUpColorSchema();
  //Animation
  for(int i = 0; i < NUM_LEDS-3; i++){
      FastLED.clear();
      leds[i] = leds_color_prep[i];
      leds[i+1] = leds_color_prep[i+1];
      leds[i+2] = leds_color_prep[i+2];
      leds[i+3] = leds_color_prep[i+3];
      FastLED.show();
      delay(20);
  }

  delay(50);
  
  for(int i = NUM_LEDS-1; i > 0; i--){
      leds[i] = leds_color_prep[i];
      FastLED.show();
      delay(20);
  }

  FastLED.clear();
  FastLED.show();
  delay(200);

  for(int i = 0; i < NUM_LEDS; i++){
      leds[i] = leds_color_prep[i];
  }
  FastLED.show();
  delay(200);

  FastLED.clear();
  FastLED.show();
  delay(200);

  for(int i = 0; i < NUM_LEDS; i++){
      leds[i] = leds_color_prep[i];
  }
  FastLED.show();
  delay(200);

  for(int i = brightness; i > 0; i--){
      FastLED.setBrightness(i);
      FastLED.show();
      delay(15);
  }

  FastLED.clear();
  FastLED.show();
  FastLED.setBrightness(brightness);

  delay(500);
}

void setStateVariables(){
  if(newData == true){ //there are new instructions from bluetooth device
    char* command = strtok(receivedChars, DELIMITER); //get main command
    if(!strcmp(command, "off")){
      mode = 0;  
    }
    else if(!strcmp(command, "fade")){
      char* s = strtok(NULL, DELIMITER); //the fade_speed in milliseconds
      if(s != NULL && atoi(s) > 0){
          method_speed = atoi(s);
          mode = 1;
      }
      else
      {
        mode = -1;
        #ifdef DEBUG
          Serial.println("Wrong parameters! <fade:s(speed in seconds)>");
        #endif  
      }
    }
    else if(!strcmp(command, "vudelay")){
      char* d = strtok(NULL, DELIMITER); //delay in milliseconds
      if(d != NULL && atoi(d)> -1){
        _delay = atoi(d);
      }
    }
    else if(!strcmp(command, "auxsensitivity")){
      char* s = strtok(NULL, DELIMITER); //sensitivity
      if(s != NULL && atoi(s) > -1){
        aux_sensitivity = atoi(s); 
      }
    }
    else if(!strcmp(command, "micsensitivity")){
      char* s = strtok(NULL, DELIMITER); //sensitivity
      char* b = strtok(NULL, DELIMITER); //bandwith
      if(s != NULL && b != NULL && atoi(s) > -1 && atoi(b) > -1 && atoi(b) < atoi(s)){
        mic_sensitivity = atoi(s);
        mic_bandwidth = atoi(b);
        mic_max = mic_sensitivity * MIC_VALUE_STEPS;
        mic_min = mic_max - mic_bandwidth * MIC_VALUE_STEPS;
      }
    }
    else if(!strcmp(command, "segmentsize")){
      char* s = strtok(NULL, DELIMITER); //segmentsize
      if(s != NULL && atoi(s) > -1 && atoi(s) <= NUM_LEDS){
        segmentSize = atoi(s);
        numOfSegments = NUM_LEDS / segmentSize;
      }
    }
    else if(!strcmp(command, "brightness")){
      char* b = strtok(NULL, DELIMITER); //the brightness (0-255)
      if(b != NULL && atoi(b) < 256 && atoi(b) > -1){
          FastLED.setBrightness(atoi(b));
      }
      else
      {
        mode = -1;
        #ifdef DEBUG
          Serial.println("Wrong parameters! <brightness:b(brightness -> value between 0 and 255)>");
        #endif  
      }
    }
    else if(!strcmp(command, "setcolor")){
      char* r = strtok(NULL, DELIMITER); //red (0-255)
      char* g = strtok(NULL, DELIMITER); //green (0-255)
      char* b = strtok(NULL, DELIMITER); //blue (0-255)
      if(r != NULL && atoi(r) < 256 && atoi(r) > -1 && g != NULL && atoi(g) < 256 && atoi(g) > -1 && b != NULL && atoi(b) < 256 && atoi(b) > -1){
        active_color = CRGB(atoi(r), atoi(g), atoi(b));
        mode = 2;
      }
      else
      {
        mode = -1;
        #ifdef DEBUG
          Serial.println("Wrong parameters! <setcolor:r:g:b(rgb -> values between 0 and 255)>");
        #endif  
      }
    }
    else if(!strcmp(command, "aux")){
      char* m = strtok(NULL, DELIMITER); //mode (0-1)
      if(m != NULL && atoi(m) < 3 && atoi(m) > -1){
        if(atoi(m) == 0){ //fade mode
          char* s = strtok(NULL, DELIMITER); //fade speed in milliseconds
          if(s != NULL && atoi(s) > -1){
            vu_mode = 0;
            mode = 3;
            color_change_speed = atoi(s);
          }
          else{
            mode = -1;
            #ifdef DEBUG
              Serial.println("Wrong parameters! <aux:0:s(fade speed -> greater than -1)>");
            #endif  
          }
        }
        else if(atoi(m) == 1){ //static color mode
           char* r = strtok(NULL, DELIMITER); //red (0-255)
           char* g = strtok(NULL, DELIMITER); //green (0-255)
           char* b = strtok(NULL, DELIMITER); //blue (0-255)
           if(r != NULL && atoi(r) < 256 && atoi(r) > -1 && g != NULL && atoi(g) < 256 && atoi(g) > -1 && b != NULL && atoi(b) < 256 && atoi(b) > -1){
             active_color = CRGB(atoi(r), atoi(g), atoi(b));
             mode = 3;
             vu_mode = 1;
           }
           else
           {
             mode = -1;
             #ifdef DEBUG
               Serial.println("Wrong parameters! <aux:1:r:g:b(rgb -> values between 0 and 255)>");
             #endif  
           }
      }
      else if(atoi(m) == 2){ //static schema mode
        vu_mode = 2;
        mode = 3;
      }
      }else{
        mode = -1;
        #ifdef DEBUG
          Serial.println("Wrong parameters! <aux:m(mode -> values between 0 and 1)>");
        #endif   
      }
    }
    else if(!strcmp(command, "mic")){
      char* m = strtok(NULL, DELIMITER); //mode (0-1)
      if(m != NULL && atoi(m) < 3 && atoi(m) > -1){
        if(atoi(m) == 0){ //fade mode
          char* s = strtok(NULL, DELIMITER); //fade speed in milliseconds
          if(s != NULL && atoi(s) > -1){
            vu_mode = 0;
            mode = 4;
            color_change_speed = atoi(s);
          }
          else{
            mode = -1;
            #ifdef DEBUG
              Serial.println("Wrong parameters! <mic:0:s(fade speed -> greater than -1)>");
            #endif  
          }
        }
        else if(atoi(m) == 1){ //static color mode
           char* r = strtok(NULL, DELIMITER); //red (0-255)
           char* g = strtok(NULL, DELIMITER); //green (0-255)
           char* b = strtok(NULL, DELIMITER); //blue (0-255)
           if(r != NULL && atoi(r) < 256 && atoi(r) > -1 && g != NULL && atoi(g) < 256 && atoi(g) > -1 && b != NULL && atoi(b) < 256 && atoi(b) > -1){
             active_color = CRGB(atoi(r), atoi(g), atoi(b));
             mode = 4;
             vu_mode = 1;
           }
           else
           {
             mode = -1;
             #ifdef DEBUG
               Serial.println("Wrong parameters! <mic:1:r:g:b(rgb -> values between 0 and 255)>");
             #endif  
           }
        }
      else if(atoi(m) == 2){ //static schema mode
        vu_mode = 2;
        mode = 4;
      }
      }else{
        mode = -1;
        #ifdef DEBUG
          Serial.println("Wrong parameters! <mic:m(mode -> values between 0 and 1)>");
        #endif   
      }
    }
    else if(!strcmp(command, "snowsparkle")){
      char* n = strtok(NULL, DELIMITER); //num of sparkles
      char* s = strtok(NULL, DELIMITER); //sparkle speed
      char* bd = strtok(NULL, DELIMITER); //brightness difference
      char* r = strtok(NULL, DELIMITER); //red (0-255)
      char* g = strtok(NULL, DELIMITER); //green (0-255)
      char* b = strtok(NULL, DELIMITER); //blue (0-255)
      if(bd != NULL && atoi(bd) >= 0 && atoi(bd) <= 100 && n != NULL && atoi(n) > 0 && atoi(n) <= NUM_LEDS && s != NULL && atoi(s) > 0 && r != NULL && atoi(r) < 256 && atoi(r) > -1 && g != NULL && atoi(g) < 256 && atoi(g) > -1 && b != NULL && atoi(b) < 256 && atoi(b) > -1){
        active_color = CRGB(atoi(r), atoi(g), atoi(b));
        num_of_sparkles = atoi(n);
        method_speed = atoi(s);
        sparkle_brightness_diff = atoi(bd)/100.0;
        mode = 5;
      }
      else
      {
        mode = -1;
        #ifdef DEBUG
          Serial.println("Wrong parameters! <sparkle:n:s:bd:r:g:b(n -> num of sparkles, s -> sparkle speed, bd -> brightness difference, rgb -> values between 0 and 255)>");
        #endif  
      }
    }
    else{
      Serial.println("NO COMMAND");
      Serial.println(receivedChars);
    }
  }
}

void setStartUpColorSchema(){
  int segment_size = NUM_LEDS / 3;
  //for-loops? should be faster
  fill_solid(leds_color_prep, segment_size*3, CRGB(255, 0, 0));
  fill_solid(leds_color_prep, segment_size*2, CRGB(0, 255, 0));
  fill_solid(leds_color_prep, segment_size, CRGB(0, 0, 255));
}

void sparkle(){
  fill_solid(leds, NUM_LEDS, CRGB(active_color.r - active_color.r*sparkle_brightness_diff, active_color.g - active_color.g*sparkle_brightness_diff, active_color.b - active_color.b*sparkle_brightness_diff)); //Set color of lamp with 90% less brightness
  for(int i = 0; i < num_of_sparkles; i++){
    int pixel = random(NUM_LEDS);
    leds[pixel] = active_color;
  }
  FastLED.show();
  delay(method_speed);
  fill_solid(leds, NUM_LEDS, CRGB(active_color.r - active_color.r*sparkle_brightness_diff, active_color.g - active_color.g*sparkle_brightness_diff, active_color.b - active_color.b*sparkle_brightness_diff)); //reset all leds
}

void auxInput(){
  int right_input = analogRead(AUX_RIGHT);
  int left_input = analogRead(AUX_LEFT);
  int value = fscale(0, aux_sensitivity, 0, numOfSegments, right_input + left_input, 0);
  music_vu(vu_mode, value, _delay);
}

void music_vu(int mode, int value, int d){
  FastLED.clear();
  switch(mode){
    case 0:
      for(int i = 0; i < value; i++){
        for(int j = 0; j < segmentSize; j++){
          leds[j+(i*segmentSize)] = leds_color_prep[j+(i*segmentSize)];
        }
      }
      if(millis()-last_color_change >= color_change_speed){
        fade_color_prep();
        last_color_change = millis();
      }
      break;
    case 1:
      fill_solid(leds, value*segmentSize, active_color);
      break;
    case 2:
      for(int i = 0; i < value; i++){
        for(int j = 0; j < segmentSize; j++){
          leds[j+(i*segmentSize)] = leds_color_prep[j+(i*segmentSize)];
        }
      }
      break;
    default:
      //TODO
      break;
  }
  FastLED.show();
  delay(d);
}

void micInput(){
  int mic_input = analogRead(MIC_PIN);
  int value = fscale(mic_min, mic_max, 0, numOfSegments, mic_input, 0);
  music_vu(vu_mode, value, _delay);
}

void fade_color_prep(){
  CRGB temp = leds_color_prep[NUM_LEDS-1];
  for(int i = NUM_LEDS-1; i > 0; i--){
    leds_color_prep[i] = leds_color_prep[i-1];
  }
  leds_color_prep[0] = temp;
}

void fade(){
  if(active_fade_color == 0){
    //Reset Color
    if(active_color.b != 0){
      active_color = CRGB(255,0,0);
    }

    active_color.r--;
    active_color.g++;

    if(active_color.g == 255 || active_color.r == 0){
      active_fade_color = 1;
      active_color = CRGB(0, 255, 0);  
    }
    
  }
  else if(active_fade_color == 1){
    //Reset Color
    if(active_color.r != 0){
      active_color = CRGB(0,255,0);
    }

    active_color.g--;
    active_color.b++;

    if(active_color.b == 255 || active_color.g == 0){
      active_fade_color = 2;
      active_color = CRGB(0, 0, 255);  
    }
  }
  else if(active_fade_color == 2){
    //Reset Color
    if(active_color.g != 0){
      active_color = CRGB(0,0,255);
    }

    active_color.b--;
    active_color.r++;

    if(active_color.r == 255 || active_color.b == 0){
      active_fade_color = 0;
      active_color = CRGB(255, 0, 0);  
    }
  }
  fill_solid(leds, NUM_LEDS, active_color);
  FastLED.show();
  delay(method_speed);
}

void error(){
  //Blink Red
  fill_solid(leds, NUM_LEDS, CRGB(255, 0, 0));
  FastLED.show();
  delay(500);
  FastLED.clear();
  FastLED.show();
  delay(500);
}

void recvData() {
    static boolean recvInProgress = false;
    static byte ndx = 0;
    char rc;
    
    while (Serial.available() > 0 && newData == false) {
        rc = Serial.read();

        if (recvInProgress == true) {
            if (rc != END_MARKER) {
                receivedChars[ndx] = rc;
                ndx++;
                if (ndx >= NUM_CHARS) {
                    ndx = NUM_CHARS - 1;
                }
            }
            else {
                receivedChars[ndx] = '\0'; // terminate the string
                recvInProgress = false;
                ndx = 0;
                newData = true;
                Serial.println(receivedChars); //ACK
            }
        }

        else if (rc == START_MARKER) {
            recvInProgress = true;
        }
    }
}

float fscale( float originalMin, float originalMax, float newBegin, float
newEnd, float inputValue, float curve){

  float OriginalRange = 0;
  float NewRange = 0;
  float zeroRefCurVal = 0;
  float normalizedCurVal = 0;
  float rangedValue = 0;
  boolean invFlag = 0;


  // condition curve parameter
  // limit range

  if (curve > 10) curve = 10;
  if (curve < -10) curve = -10;

  curve = (curve * -.1) ; // - invert and scale - this seems more intuitive - postive numbers give more weight to high end on output 
  curve = pow(10, curve); // convert linear scale into lograthimic exponent for other pow function

  /*
   Serial.println(curve * 100, DEC);   // multply by 100 to preserve resolution  
   Serial.println(); 
   */

  // Check for out of range inputValues
  if (inputValue < originalMin) {
    inputValue = originalMin;
  }
  if (inputValue > originalMax) {
    inputValue = originalMax;
  }

  // Zero Refference the values
  OriginalRange = originalMax - originalMin;

  if (newEnd > newBegin){ 
    NewRange = newEnd - newBegin;
  }
  else
  {
    NewRange = newBegin - newEnd; 
    invFlag = 1;
  }

  zeroRefCurVal = inputValue - originalMin;
  normalizedCurVal  =  zeroRefCurVal / OriginalRange;   // normalize to 0 - 1 float

  /*
  Serial.print(OriginalRange, DEC);  
   Serial.print("   ");  
   Serial.print(NewRange, DEC);  
   Serial.print("   ");  
   Serial.println(zeroRefCurVal, DEC);  
   Serial.println();  
   */

  // Check for originalMin > originalMax  - the math for all other cases i.e. negative numbers seems to work out fine 
  if (originalMin > originalMax ) {
    return 0;
  }

  if (invFlag == 0){
    rangedValue =  (pow(normalizedCurVal, curve) * NewRange) + newBegin;

  }
  else     // invert the ranges
  {   
    rangedValue =  newBegin - (pow(normalizedCurVal, curve) * NewRange); 
  }

  return rangedValue;
}
